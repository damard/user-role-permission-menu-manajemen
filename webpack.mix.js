let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .mix.js('resources/assets/js/core.js', 'public/js/plugin');
  //  .mix.js('resources/assets/js/bootstrap.js', 'public/js/plugin');
  //  .mix.less('resources/assets/less/_main_full/bootstrap.less', 'public/css')
  //  .mix.less('resources/assets/less/_main_full/colors.less', 'public/css')
  //  .mix.less('resources/assets/less/_main_full/components.less', 'public/css')
  // //  .sass('resources/assets/sass/app.scss', 'public/css')
  //  .mix.less('resources/assets/less/_main_full/core.less', 'public/css');
