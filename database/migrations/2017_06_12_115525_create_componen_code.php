<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponenCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('com_code', function (Blueprint $table) {
            $table->string('com_cd');
            $table->string('code_nm');
            $table->string('code_group')->nullable();
            $table->string('code_value')->nullable();
            $table->timestamps();
            $table->primary('com_cd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('com_code');
    }
}
