<?php

use Illuminate\Database\Seeder;
use App\Models\MenuItem;
class MenuItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->truncateData();
      $insert = new MenuItem;
      $insert->menu_id = 1;
      $insert->mmenuitem_name = 'Rawat Jalan';
      $insert->link = 'registrasi/rawat-jalan';
      $insert->permission_id = 34;
      $insert->order_id = 1;
      $insert->save();
      $insert = new MenuItem;
      $insert->menu_id = 1;
      $insert->mmenuitem_name = 'Rawat Inap';
      $insert->link = 'registrasi/rawat-inap';
      $insert->permission_id = 38;
      $insert->order_id = 2;
      $insert->save();
    }
    public function truncateData()
    {
      DB::statement("truncate menu_items CASCADE");
    }
}
