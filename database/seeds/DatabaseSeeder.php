<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LaratrustSeeder::class);
        $this->call(LanguageSeeder::class);
        $this->call(NationSeeder::class);
        $this->call(StoreProcedureSeeder::class);
        $this->call(ViewSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(MenuItemSeeder::class);
    }
}
