<?php

use Illuminate\Database\Seeder;
use App\Models\ViewQuery;
class ViewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $run = new ViewQuery;
        $run->executeQueryView();
    }
}
