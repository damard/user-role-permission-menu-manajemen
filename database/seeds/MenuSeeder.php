<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;
class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->truncateData();
      $insert = new Menu;
      $insert->menu_name ='Registrasi';
      $insert->save();
    }
    public function truncateData()
    {
      DB::table('menus')->truncate();
    }
}
