<?php

use Illuminate\Database\Seeder;
use App\Models\ComLanguage;
class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateData();
        $insert = new ComLanguage;
        $insert->lang_id ='en';
        $insert->lang_name ='English';
        $insert->flag_path ='us.png';
        $insert->save();
        $insert = new ComLanguage;
        $insert->lang_id ='id';
        $insert->lang_name ='Indonesia';
        $insert->flag_path = 'id.png';
        $insert->save();
    }
    public function truncateData()
    {
      DB::table('com_languages')->truncate();
    }
}
