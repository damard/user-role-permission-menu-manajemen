<?php

use Illuminate\Database\Seeder;

use App\Models\StoreProcedure;
class StoreProcedureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $run = new StoreProcedure;
        $run->executeStoreProcedure();
    }
}
