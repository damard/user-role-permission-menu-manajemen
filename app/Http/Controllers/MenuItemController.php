<?php

namespace App\Http\Controllers;

use App\Models\MenuItem;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;

class MenuItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $permissionname = preg_replace("/[^A-Za-z ]/", '', $request->mmenuitem_name);
      $simpan = DB::transaction(function () use($request,$permissionname) {
              $insert = new Permission;
              $insert->name = 'read-'.$permissionname;
              $insert->display_name = 'Read '.ucwords($permissionname);
              $insert->description = 'Read '.ucwords($permissionname);
              $insert->save();
              $permission = $insert;
              $role = Role::where('name','superadministrator')->get()->first();
              if(!$role->hasPermission($permission->name)){
                $role->attachPermission($permission);
              }
              $insert = new Permission;
              $insert->name = 'create-'.$permissionname;
              $insert->display_name = 'Create '.ucwords($permissionname);
              $insert->description = 'Create '.ucwords($permissionname);
              $insert->save();

              $insert = new Permission;
              $insert->name = 'delete-'.$permissionname;
              $insert->display_name = 'Delete '.ucwords($permissionname);
              $insert->description = 'Delete '.ucwords($permissionname);
              $insert->save();

              $insert = new Permission;
              $insert->name = 'update-'.$permissionname;
              $insert->display_name = 'Update '.ucwords($permissionname);
              $insert->description = 'Update '.ucwords($permissionname);
              $insert->save();

              $insert = new MenuItem;
              $insert->menu_id = $request->menu_id;
              $insert->mmenuitem_name = $request->mmenuitem_name;
              $insert->parent = $request->parent;
              $insert->link = $request->link;
              $insert->permission_id = $permission->id;
              $insert->order_id = $request->order_id;
              $insert->save();
              return $insert;
              });
        if( request()->wantsJson() )
        {
          return $simpan;
        }
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function show(MenuItem $menuItem)
    {
        return $menuItem;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function edit(MenuItem $menuItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MenuItem $menuItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(MenuItem $menuItem)
    {
        $menuItem->delete();
        return redirect()->back();
    }
}
