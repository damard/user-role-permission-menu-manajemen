<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use App\Models\ComNation;
use Illuminate\Http\Request;
// use App\Http\Requests\UpdateProfile;
use App\Http\Requests\ProfileUpdate;

class ProfileController extends Controller
{
  protected $prefixDirectory='public.profile';
    public function index()
    {
      $data = User::find(Auth::user()->id);
      $nation = ComNation::all();
      $hasil['data']=$data;
      $hasil['nation']= $nation;
      // return $hasil;
      if( request()->wantsJson() )
      {
        return $hasil;
      }
      return view($this->prefixDirectory.'.'.'index',compact('data','nation'));
    }
    public function update(ProfileUpdate $request, $id)
    {
      // return $request->all();
      $update = User::find($id);
      $update->username = $request->username;
      $update->name = $request->name;
      $update->alamat = $request->alamat;
      $update->desa = $request->desa;
      $update->kecamatan = $request->kecamatan;
      $update->kabupaten = $request->kabupaten;
      $update->provinsi = $request->provinsi;
      $update->kode_post = $request->kode_post;
      $update->email = $request->email;
      $update->negara = $request->negara;
      $update->telepon = $request->telepon;
      $update->foto = $request->foto;
      $update->save();
      if( request()->wantsJson() )
      {
        return $update;
      }
      return redirect()->back();
    }
}
