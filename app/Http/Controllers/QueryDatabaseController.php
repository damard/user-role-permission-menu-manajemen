<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\StoreProcedure;
use App\Models\ViewQuery;

class QueryDatabaseController extends Controller
{
    public function executeQueryStoreProcedure(StoreProcedure $query)
    {
      $run = $query->executeStoreProcedure();
      if($run){
        return 'Query Success To Execute';
      }
      return 'Query Failed To Execute';
    }

    public function executeQueryViewQuery(StoreProcedure $query)
    {
      $run = $query->executeQueryView();
      if($run){
        return 'Query Success To Execute';
      }
      return 'Query Failed To Execute';
    }
    public function executeCustomView(Request $request)
    {
      if ($_ENV['DB_PASSWORD'] == $request->db_password) {
        $execute = new ViewQuery;
        $execute->executeCustomQuery($request->query);
      }
      return redirect()->back();
    }
}
