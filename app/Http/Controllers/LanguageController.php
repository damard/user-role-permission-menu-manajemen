<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\ComLanguage;
class LanguageController extends Controller
{
    public function setLanguage(Request $request)
    {
      if(Request::ajax()){
        $data = ComLanguage::find($request->id);
        $request->session()->put('langauge', $data->lang_id);
        $request->session()->put('icon', $data->flag_path);
      }
    }
}
