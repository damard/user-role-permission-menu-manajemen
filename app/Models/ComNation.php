<?php

namespace App\models;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;

class ComNation extends Model
{
  use LogsActivity;
    protected $table ='com_nation';
    protected static $logAttributes = ['nation_cd', 'nation_nm', 'capital'];
}
