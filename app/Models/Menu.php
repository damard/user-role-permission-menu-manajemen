<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\MenuItem;

class Menu extends Model
{
    use LogsActivity;
    protected $table ='menus';
    protected static $logAttributes = ['menu_name'];

    function menuItems()
    {
      return $this->hasMany('App\Models\MenuItem');
    }

}
