<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class StoreProcedure extends Model
{
    //taruh semua query store procedure di sini
    /*
    * Gunakan DB::statement untuk execusi query
    * Satu DB::statement mewakili satu fungsi satau satu view
    */
    public function executeStoreProcedure()
    {
      //cek apakah memiliki sp tertentu
      $cek = DB::select(DB::raw("SELECT EXISTS (
            SELECT *
            FROM pg_catalog.pg_proc
            JOIN pg_namespace ON pg_catalog.pg_proc.pronamespace = pg_namespace.oid
            WHERE proname = 'f_delfunc'
                AND pg_namespace.nspname = 'public'
            )"));
      if($cek[0]->exists){
        //hapus semua schema
        DB::select(DB::raw("select f_delfunc('public')"));
      }else{
        // buat fungsi untuk menghapus semua sp
        DB::statement(
          'CREATE OR REPLACE FUNCTION f_delfunc(_sch text)
          RETURNS void AS
          $function$
          DECLARE
             _sql text;
          BEGIN
             SELECT INTO _sql
                    string_agg(format('."'".'DROP FUNCTION %s(%s);'."'".'
                                    , p.oid::regproc
                                    , pg_get_function_identity_arguments(p.oid))
                             , E'."' \n'".')
             FROM   pg_proc      p
             JOIN   pg_namespace ns ON ns.oid = p.pronamespace
             WHERE  ns.nspname = _sch;

             IF _sql IS NOT NULL THEN
                EXECUTE _sql;
             END IF;
          END
          $function$
          LANGUAGE plpgsql;
                ');
      }
      //jalankan sp terbaru
    
    }
}
