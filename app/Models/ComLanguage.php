<?php

namespace App\Models;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;

class ComLanguage extends Model
{
  use LogsActivity;
  protected $table = 'com_languages';
  protected static $logAttributes = ['lang_id', 'lang_name', 'flag_path'];
}
