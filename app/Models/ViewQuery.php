<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
class ViewQuery extends Model
{
  use LogsActivity;
  protected $table='core_query';
  protected static $logAttributes = ['name', 'email', 'password'];
  public function executeCustomQuery($value)
  {
    DB::statement($value);
  }
  /*
  * Gunakan DB::statement untuk execusi query
  * Satu DB::statement mewakili satu fungsi satau satu view
  */
  public function executeQueryView()
  {
    DB::statement("SELECT 'DROP VIEW ' || t.oid::regclass || ';'
                    FROM   pg_class t
                    JOIN   pg_namespace n ON n.oid = t.relnamespace
                    WHERE  t.relkind = 'v'
                    AND    n.nspname = 'public'
                    ORDER  BY 1;
                    ");
  DB::statement("CREATE OR REPLACE VIEW public.vw_menus AS
 SELECT DISTINCT menu.id,
 	menu.menu_item_id,
    menu.username,
    menu.menu_name,
    menu.link_menu,
    menu.css_icon,
    menu.icon_foto,
    menu.mmenuitem_name,
    menu.link,
    menu.parent,
    menu.menu_id,
    menu.order_id,
    menu.publish
   FROM ( SELECT u.id,
   			mi.id as menu_item_id,
            u.username,
            m.menu_name,
            m.css_icon,
            m.icon_foto,
            m.link as link_menu,
            mi.mmenuitem_name,
            mi.link,
            mi.parent,
            mi.menu_id,
            mi.order_id,
            m.publish
           FROM users u
             JOIN permission_user pu ON u.id = pu.user_id
             JOIN permissions p ON pu.permission_id = p.id
             JOIN menu_items mi ON mi.permission_id = p.id
             JOIN menus m ON m.id = mi.menu_id
        UNION ALL
         SELECT u.id,
         	mi.id as menu_item_id,
            u.username,
            m.menu_name,
            m.css_icon,
            m.icon_foto,
            m.link as link_menu,
            mi.mmenuitem_name,
            mi.link,
            mi.parent,
            mi.menu_id,
            mi.order_id,
            m.publish
           FROM users u
             JOIN role_user ru ON ru.user_id = u.id
             JOIN roles r ON ru.role_id = r.id
             JOIN permission_role pr ON pr.role_id = r.id
             JOIN permissions p ON p.id = pr.permission_id
             JOIN menu_items mi ON mi.permission_id = p.id
             JOIN menus m ON m.id = mi.menu_id) menu");
  }
}
