<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewMenu extends Model
{
    protected $table ='vw_menus';

    public function childMenu()
    {
      return $this->hasOne('App\Models\ViewMenu','parent','menu_item_id');
    }
}
