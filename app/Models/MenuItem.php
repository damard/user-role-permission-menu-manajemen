<?php

namespace App\Models;

use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;


class MenuItem extends Model
{
    use LogsActivity;
    protected $table ='menu_items';
    protected static $logAttributes = ['menu_id', 'mmenuitem_name', 'parent','link','permission_id','order_id'];

    public function menu()
    {
      $this->belongsTo('App\Models\Menu');
    }

}
