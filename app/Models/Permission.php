<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laratrust\LaratrustPermission;

class Permission extends LaratrustPermission
{
    public function menuitem()
    {
        $this->hasOne('App\Models\MenuItem');
    }
    // public function getPermission()
    // {
    //   return $this->morphTo();
    // }
}
