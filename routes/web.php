<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'execute'],function(){
  Route::get('store-procedure','QueryDatabaseController@executeQueryStoreProcedure');
  Route::get('view','QueryDatabaseController@executeQueryViewQuery');
});

Auth::routes();
Route::group(['middleware'=>['auth']],function(){
  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/user','UserController@index');
  Route::resource('profile','ProfileController');
});
Route::group(['middleware'=>['role:superadministrator']],function(){
  Route::get('menu','MenuController@index');
  Route::post('menu','MenuController@store');
  Route::delete('menu/{id}','MenuController@destroy');

  //menuItems
  Route::post('menu-item','MenuItemController@store');
});
