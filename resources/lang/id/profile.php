<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name'      => 'Nama Lenkap',
    'username'  => 'Username',
    'addres'    =>'Alamat',
    'village'   =>'Desa',
    'city'      => 'Kota',
    'state'     => 'Provinsi',
    'district'  => 'Kecamatan',
    'zip'       => 'Kode Pos',
    'email'     => 'Email',
    'country'   => 'Negara',
    'phone'     => 'Telepon',
    'photoProfile'     => 'Upload foto profile',
    'current-password'     => 'Password Lama',
    'new-password'     => 'Password Baru',
    'throttle'  => 'Too many login attempts. Please try again in :seconds seconds.',

];
