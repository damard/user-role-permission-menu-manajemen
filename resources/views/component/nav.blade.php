<div class="sidebar sidebar-secondary sidebar-default">
  <div class="sidebar-content">

    {{-- <!-- Sidebar search -->
    <div class="sidebar-category">
      <div class="category-title">
        <span>Search</span>
        <ul class="icons-list">
          <li><a href="#" data-action="collapse"></a></li>
        </ul>
      </div>

      <div class="category-content">
        <form action="#">
          <div class="has-feedback has-feedback-left">
            <input type="search" class="form-control" placeholder="Search">
            <div class="form-control-feedback">
              <i class="icon-search4 text-size-base text-muted"></i>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- /sidebar search --> --}}
@php
  $menuSecondary = \App\Models\ViewMenu::select('mmenuitem_name','link')->where('id',Auth::user()->id)
  ->where('menu_name',$menu)->get();
@endphp

    <!-- Sub navigation -->
    <div class="sidebar-category">
      <div class="category-title">
        <span>Navigation</span>
        <ul class="icons-list">
          <li><a href="#" data-action="collapse"></a></li>
        </ul>
      </div>

      <div class="category-content no-padding">
        <ul class="navigation navigation-alt navigation-accordion">
          <li class="navigation-header">Category title</li>
          @foreach ($menuSecondary as $key)
            <li><a href="{{$key->link}}"><i class="icon-googleplus5"></i>{{$key->mmenuitem_name}}</a></li>
          @endforeach

        </ul>
      </div>
    </div>
    {{ $optional or '' }}
    <!-- /sub navigation -->


    <!-- Form sample -->
    {{-- <div class="sidebar-category">
      <div class="category-title">
        <span>Form example</span>
        <ul class="icons-list">
          <li><a href="#" data-action="collapse"></a></li>
        </ul>
      </div>

      <form action="#" class="category-content">
        <div class="form-group">
          <label>Your name:</label>
          <input type="text" class="form-control" placeholder="Username">
        </div>

        <div class="form-group">
          <label>Your password:</label>
          <input type="password" class="form-control" placeholder="Password">
        </div>

        <div class="form-group">
          <label>Your message:</label>
          <textarea rows="3" cols="3" class="form-control" placeholder="Default textarea"></textarea>
        </div>

        <div class="row">
          <div class="col-xs-6">
            <button type="reset" class="btn btn-danger btn-block">Reset</button>
          </div>
          <div class="col-xs-6">
            <button type="submit" class="btn btn-info btn-block">Submit</button>
          </div>
        </div>
      </form>
    </div> --}}
    <!-- /form sample -->

  </div>
</div>
