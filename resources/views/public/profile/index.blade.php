@extends('master')

@section('css')
  <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
@stop
@section('secondary-navbar')
  @component('component.nav')
    @slot('menu')
      Profile
    @endslot
    @slot('optional')

    @endslot
  @endcomponent
@stop
@section('content')
  {{-- <div class="row">
  	<div class="col-lg-9">
  		<!-- taruh yang di butuhkan di sini -->
  		<div class="tab-content">
  			<div role ="tabpanel" class="tab-pane active" id="profile">
  				<div class="panel panel-flat">
  					<div class="panel-heading">
  						<h6 class="panel-title">Profile information</h6>
  						<div class="heading-elements">
  							<ul class="icons-list">
  								<li>
  									<a data-action="collapse"></a>
  								</li>
  								<li>
  									<a data-action="reload"></a>
  								</li>
  								<li>
  									<a data-action="close"></a>
  								</li>
  							</ul>
  						</div>
  					</div>
  					<div class="panel-body">
  						<form action="{{route('profile.update',$data->id)}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="put">
  							<div class="form-group">
  								<div class="row">
  									<div class="col-md-6">
  										<label>@lang('profile.username')</label>
  										<input type="text" value="{{$data->username}}" name="username" class="form-control"></div>
  									<div class="col-md-6">
  										<label>@lang('profile.name')</label>
  										<input type="text" value="{{$data->name}}" name="name" class="form-control">
                    </div>
  								</div>
  							</div>
  							<div class="form-group">
  								<div class="row">
  									<div class="col-md-4">
  										<label>@lang('profile.addres')</label>
  										<input type="text" value="{{$data->alamat}}" name="alamat" class="form-control"></div>
  									<div class="col-md-4">
  										<label>@lang('profile.village')</label>
  										<input type="text" value="{{$data->desa}}" name="desa" class="form-control"></div>
                    <div class="col-md-4">
  										<label>@lang('profile.district')</label>
  										<input type="text" value="{{$data->kecamatan}}" name="kecamatan" class="form-control"></div>
  								</div>
  							</div>
  							<div class="form-group">
  								<div class="row">
  									<div class="col-md-4">
  										<label>@lang('profile.city')</label>
  										<input type="text" value="{{$data->kabupaten}}" name="kabupaten" class="form-control"></div>
  									<div class="col-md-4">
  										<label>@lang('profile.state')</label>
  										<input type="text" value="{{$data->provinsi}}" name="provinsi" class="form-control"></div>
  									<div class="col-md-4">
  										<label>@lang('profile.zip')</label>
  										<input type="text" value="{{$data->kode_post}}" name="kode_post" class="form-control"></div>
  								</div>
  							</div>
  							<div class="form-group">
  								<div class="row">
  									<div class="col-md-6">
  										<label>@lang('profile.email')</label>
  										<input type="email" value="{{$data->email}}" name="email" class="form-control"></div>
  									<div class="col-md-6">
  										<label>@lang('profile.country')</label>
  										<select class="select form-control" id="select" name="negara">
                        @foreach ($nation as $key)
                          <option value="{{$key->id}}" {{$key->id == $data->negara? 'selected':'' }}>{{$key->nation_nm}}</option>
                        @endforeach
  										</select>
  									</div>
  								</div>
  							</div>
  							<div class="form-group">
  								<div class="row">
  									<div class="col-md-6">
  										<label>@lang('profile.phone')</label>
  										<input type="text" value="{{$data->telepon}}" name="telepon" class="form-control">
  										<span class="help-block">+99-99-9999-9999</span>
  									</div>
  									<div class="col-md-6">
  										<label class="display-block">@lang('profile.photoProfile')</label>
                      <div class="input-group">
                         <span class="input-group-btn">
                           <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                             <i class="fa fa-picture-o"></i> Choose
                           </a>
                         </span>
                         <input id="thumbnail" class="form-control" type="text" value="{{$data->foto}}" name="foto"></div>
                         <img id="holder" src="{{asset($data->foto)}}" style="margin-top:15px;max-height:100px;">
  									</div>
  								</div>
  							</div>
  							<div class="text-right">
  								<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
  							</div>
  						</form>
  					</div>
  				</div>
  			</div>

  			<div role="tabpanel" class="tab-pane" id="account-setting">
  				<div class="panel panel-flat">
  					<div class="panel-heading">
  						<h6 class="panel-title">Account settings</h6>
  						<div class="heading-elements">
  							<ul class="icons-list">
  								<li>
  									<a data-action="collapse"></a>
  								</li>
  								<li>
  									<a data-action="reload"></a>
  								</li>
  								<li>
  									<a data-action="close"></a>
  								</li>
  							</ul>
  						</div>
  					</div>
  					<div class="panel-body">
  						<form action="#">
  							<div class="form-group">
  								<div class="row">
  									<div class="col-md-6">
  										<label>@lang('profile.username')</label>
  										<input type="text" value="Kopyov" readonly="readonly" class="form-control"></div>
  									<div class="col-md-6">
  										<label>@lang('profile.current-password')</label>
  										<input type="password" value="password" readonly="readonly" class="form-control"></div>
  								</div>
  							</div>
  							<div class="form-group">
  								<div class="row">
  									<div class="col-md-6">
  										<label>@lang('profile.new-password')</label>
  										<input type="password" placeholder="Enter new password" class="form-control"></div>
  									<div class="col-md-6">
  										<label>>@lang('profile.new-password')</label>
  										<input type="password" placeholder="Repeat new password" class="form-control"></div>
  								</div>
  							</div>
  							<div class="form-group">
  								<div class="row">
  									<div class="col-md-6">
  										<label>Profile visibility</label>
  										<div class="radio">
  											<label>
  											<input type="radio" name="visibility" class="styled" checked="checked">Visible to everyone</label>
  										</div>
  										<div class="radio">
  											<label>
  											<input type="radio" name="visibility" class="styled">Visible to friends only</label>
  										</div>
  										<div class="radio">
  											<label>
  											<input type="radio" name="visibility" class="styled">Visible to my connections only</label>
  										</div>
  										<div class="radio">
  											<label>
  											<input type="radio" name="visibility" class="styled">Visible to my colleagues only</label>
  										</div>
  									</div>
  									<div class="col-md-6">
  										<label>Notifications</label>
  										<div class="checkbox">
  											<label>
  											<input type="checkbox" class="styled" checked="checked">Password expiration notification</label>
  										</div>
  										<div class="checkbox">
  											<label>
  											<input type="checkbox" class="styled" checked="checked">New message notification</label>
  										</div>
  										<div class="checkbox">
  											<label>
  											<input type="checkbox" class="styled" checked="checked">New task notification</label>
  										</div>
  										<div class="checkbox">
  											<label>
  											<input type="checkbox" class="styled">New contact request notification</label>
  										</div>
  									</div>
  								</div>
  							</div>
  							<div class="text-right">
  								<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
  							</div>
  						</form>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  	<!-- sampai sini -->
  	<div class="col-lg-3">
  		<!-- User thumbnail -->
  		<div class="thumbnail">
  			<div class="thumb thumb-rounded thumb-slide">
  				<img src="{{$data->foto ?? '/images/placeholder.jpg'}}" alt="">
  				<div class="caption">
  					<span>
  					<a href="#" class="btn bg-success-400 btn-icon btn-xs" data-popup="lightbox"><i class="icon-plus2"></i></a>
  					<a href="user_pages_profile.html" class="btn bg-success-400 btn-icon btn-xs"><i class="icon-link"></i></a>
  					</span>
  				</div>
  			</div>
  			<div class="caption text-center">
  				<h6 class="text-semibold no-margin">Hanna Dorman <small class="display-block">UX/UI designer</small></h6>
  				<ul class="icons-list mt-15">
  					<li>
  						<a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a>
  					</li>
  					<li>
  						<a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a>
  					</li>
  					<li>
  						<a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a>
  					</li>
  				</ul>
  			</div>
  		</div>
  		<!-- /user thumbnail -->
  		<!-- Navigation -->
  		<div class="panel panel-flat">
  			<div class="panel-heading">
  				<h6 class="panel-title">Navigation</h6>
  				<div class="heading-elements">
  					<a href="#" class="heading-text">See all &rarr;</a>
  				</div>
  			</div>
  			<div class="list-group no-border no-padding-top" role="tablist">
  				<a href="#profile" data-trigger="tab" class="list-group-item"><i class="icon-user"></i> My profile</a>
  				<a href="#" class="list-group-item"><i class="icon-cash3"></i> Balance</a>
  				<a href="#" class="list-group-item"><i class="icon-tree7"></i> Connections <span class="badge bg-danger pull-right">29</span></a>
  				<a href="#" class="list-group-item"><i class="icon-users"></i> Friends</a>
  				<div class="list-group-divider"></div>
  				<a href="#" class="list-group-item"><i class="icon-calendar3"></i> Events <span class="badge bg-teal-400 pull-right">48</span></a>
  				<a href="#account-setting" aria-controls="account-setting" role="tab" data-trigger="tab" class="list-group-item"><i class="icon-cog3"></i> Account settings</a>
  			</div>
  		</div>
  		<!-- /navigation -->
  		<!-- Share your thoughts -->
  		<div class="panel panel-flat">
  			<div class="panel-heading">
  				<h6 class="panel-title">Share your thoughts</h6>
  				<div class="heading-elements">
  					<ul class="icons-list">
  						<li>
  							<a data-action="close"></a>
  						</li>
  					</ul>
  				</div>
  			</div>
  			<div class="panel-body">
  				<form action="#">
  					<div class="form-group">
  						<textarea name="enter-message" class="form-control mb-15" rows="3" cols="1" placeholder="What's on your mind?"></textarea>
  					</div>
  					<div class="row">
  						<div class="col-xs-6">
  							<ul class="icons-list icons-list-extended mt-10">
  								<li>
  									<a href="#" data-popup="tooltip" title="Add photo" data-container="body"><i class="icon-images2"></i></a>
  								</li>
  								<li>
  									<a href="#" data-popup="tooltip" title="Add video" data-container="body"><i class="icon-film2"></i></a>
  								</li>
  								<li>
  									<a href="#" data-popup="tooltip" title="Add event" data-container="body"><i class="icon-calendar2"></i></a>
  								</li>
  							</ul>
  						</div>
  						<div class="col-xs-6 text-right">
  							<button type="button" class="btn btn-primary btn-labeled btn-labeled-right">Share <b><i class="icon-circle-right2"></i></b></button>
  						</div>
  					</div>
  				</form>
  			</div>
  		</div>
  		<!-- /share your thoughts -->
  		<!-- Balance chart -->
  		<div class="panel panel-flat">
  			<div class="panel-heading">
  				<h6 class="panel-title">Balance changes</h6>
  				<div class="heading-elements">
  					<span class="heading-text"><i class="icon-arrow-down22 text-danger"></i><span class="text-semibold">- 29.4%</span></span>
  				</div>
  			</div>
  			<div class="panel-body">
  				<div class="chart-container">
  					<div class="chart" id="visits" style="height: 300px;"></div>
  				</div>
  			</div>
  		</div>
  		<!-- /balance chart -->
  		<!-- Connections -->
  		<div class="panel panel-flat">
  			<div class="panel-heading">
  				<h6 class="panel-title">Latest connections</h6>
  				<div class="heading-elements">
  					<span class="badge badge-success heading-text">+32</span>
  				</div>
  			</div>
  			<ul class="media-list media-list-linked pb-5">
  				<li class="media-header">Office staff</li>
  				<li class="media">
  					<a href="#" class="media-link">
  					<div class="media-left">
  						<img src="/images/placeholder.jpg" class="img-circle" alt="">
  					</div>
  					<div class="media-body">
  						<span class="media-heading text-semibold">James Alexander</span>
  						<span class="media-annotation">UI/UX expert</span>
  					</div>
  					<div class="media-right media-middle">
  						<span class="status-mark bg-success"></span>
  					</div>
  					</a>
  				</li>
  				<li class="media">
  					<a href="#" class="media-link">
  					<div class="media-left">
  						<img src="/images/placeholder.jpg" class="img-circle" alt="">
  					</div>
  					<div class="media-body">
  						<span class="media-heading text-semibold">Jeremy Victorino</span>
  						<span class="media-annotation">Senior designer</span>
  					</div>
  					<div class="media-right media-middle">
  						<span class="status-mark bg-danger"></span>
  					</div>
  					</a>
  				</li>
  				<li class="media">
  					<a href="#" class="media-link">
  					<div class="media-left">
  						<img src="/images/placeholder.jpg" class="img-circle" alt="">
  					</div>
  					<div class="media-body">
  						<div class="media-heading">
  							<span class="text-semibold">Jordana Mills</span>
  						</div>
  						<span class="text-muted">Sales consultant</span>
  					</div>
  					<div class="media-right media-middle">
  						<span class="status-mark bg-grey-300"></span>
  					</div>
  					</a>
  				</li>
  				<li class="media">
  					<a href="#" class="media-link">
  					<div class="media-left">
  						<img src="/images/placeholder.jpg" class="img-circle" alt="">
  					</div>
  					<div class="media-body">
  						<div class="media-heading">
  							<span class="text-semibold">William Miles</span>
  						</div>
  						<span class="text-muted">SEO expert</span>
  					</div>
  					<div class="media-right media-middle">
  						<span class="status-mark bg-success"></span>
  					</div>
  					</a>
  				</li>
  				<li class="media-header">Partners</li>
  				<li class="media">
  					<a href="#" class="media-link">
  					<div class="media-left">
  						<img src="/images/placeholder.jpg" class="img-circle" alt="">
  					</div>
  					<div class="media-body">
  						<span class="media-heading text-semibold">Margo Baker</span>
  						<span class="media-annotation">Google</span>
  					</div>
  					<div class="media-right media-middle">
  						<span class="status-mark bg-success"></span>
  					</div>
  					</a>
  				</li>
  				<li class="media">
  					<a href="#" class="media-link">
  					<div class="media-left">
  						<img src="/images/placeholder.jpg" class="img-circle" alt="">
  					</div>
  					<div class="media-body">
  						<span class="media-heading text-semibold">Beatrix Diaz</span>
  						<span class="media-annotation">Facebook</span>
  					</div>
  					<div class="media-right media-middle">
  						<span class="status-mark bg-warning-400"></span>
  					</div>
  					</a>
  				</li>
  				<li class="media">
  					<a href="#" class="media-link">
  					<div class="media-left">
  						<img src="/images/placeholder.jpg" class="img-circle" alt="">
  					</div>
  					<div class="media-body">
  						<span class="media-heading text-semibold">Richard Vango</span>
  						<span class="media-annotation">Microsoft</span>
  					</div>
  					<div class="media-right media-middle">
  						<span class="status-mark bg-grey-300"></span>
  					</div>
  					</a>
  				</li>
  			</ul>
  		</div>
  		<!-- /connections -->
  	</div>
  </div> --}}

  <editprofile></editprofile>
@endsection
@push('scripts')
<script src="{{asset('js/plugin/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugin/custom/my-custom.js')}}"></script>
@endpush
