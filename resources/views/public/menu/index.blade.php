@extends('master')

@section('css')

@stop
@section('secondary-navbar')
  @component('component.nav')
    @slot('menu')
      Settings
    @endslot
    @slot('optional')

    @endslot
  @endcomponent
@stop
@section('right-bradcrump')
    <li><a href="#" data-toggle="modal" data-target="#tambahMenu"><i class="icon-plus-circle2 position-left"></i> Tambah Menu</a></li>
@endsection

@section('content')
<manajemenmenu></manajemenmenu>
  {{-- <div class="row">
  	<div class="col-lg-4">
      <div class="tab-content">
  			<div role ="tabpanel" class="tab-pane active" id="profile">
  				<div class="panel panel-flat">
  					<div class="panel-heading">
  						<h6 class="panel-title">Menu</h6>
  						<div class="heading-elements">
  							<ul class="icons-list">
  								<li>
  									<a data-action="collapse"></a>
  								</li>
  								<li>
  									<a data-action="reload"></a>
  								</li>
  								<li>
  									<a data-action="close"></a>
  								</li>
  							</ul>
  						</div>
  					</div>
  					<div class="panel-body">
  						fdsfdsf
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  	<!-- sampai sini -->
  	<div class="col-lg-4">
      <div class="tab-content">
  			<div role ="tabpanel" class="tab-pane active" id="profile">
  				<div class="panel panel-flat">
  					<div class="panel-heading">
  						<h6 class="panel-title">Menu Item</h6>
  						<div class="heading-elements">
  							<ul class="icons-list">
  								<li>
  									<a data-action="collapse"></a>
  								</li>
  								<li>
  									<a data-action="reload"></a>
  								</li>
  								<li>
  									<a data-action="close"></a>
  								</li>
  							</ul>
  						</div>
  					</div>
  					<div class="panel-body">
  						fdsfdsf
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
    <div class="col-lg-4">
      <div class="tab-content">
  			<div role ="tabpanel" class="tab-pane active" id="profile">
  				<div class="panel panel-flat">
  					<div class="panel-heading">
  						<h6 class="panel-title">Menu Generate</h6>
  						<div class="heading-elements">
  							<ul class="icons-list">
  								<li>
  									<a data-action="collapse"></a>
  								</li>
  								<li>
  									<a data-action="reload"></a>
  								</li>
  								<li>
  									<a data-action="close"></a>
  								</li>
  							</ul>
  						</div>
  					</div>
  					<div class="panel-body">
  						fdsfdsf
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>

    <div class="col-lg-4">
      <div class="tab-content">
        <div role ="tabpanel" class="tab-pane active" id="profile">
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h6 class="panel-title">Menu Generate</h6>
              <div class="heading-elements">
                <ul class="icons-list">
                  <li>
                    <a data-action="collapse"></a>
                  </li>
                  <li>
                    <a data-action="reload"></a>
                  </li>
                  <li>
                    <a data-action="close"></a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="panel-body">
              fdsfdsf
            </div>
          </div>
        </div>
      </div>
    </div>

  </div> --}}
@endsection
@push('scripts')
{{-- <script type="text/javascript" src="{{asset('js/plugin/custom/my-custom.js')}}"></script> --}}
<script type="text/javascript">
  $(document).ready(function(){
    $('#lfm').filemanager('image');
  });
</script>
@endpush
