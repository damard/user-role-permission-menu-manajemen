<div class="page-header page-header-default">
  <div class="page-header-content">
    <div class="page-title">
      <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Starters</span> - Dual Sidebars</h4>
    </div>

    <div class="heading-elements">
      <a href="#" class="btn btn-labeled btn-labeled-right bg-blue heading-btn">Button <b><i class="icon-menu7"></i></b></a>
    </div>
  </div>

  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <?php $url='';?>
    @for($i = 0; $i <= count(Request::segments()); $i++)
      @if($i == count(Request::segments()))
        <?php $url =$url.'/'.Request::segment($i); ?>
        @if($i==1)
            <li><a href="{{$url}}"><i class="icon-home2 position-left active"></i>{{ucwords(Request::segment($i))}}</a></li>
        @else
          <li><a href="{{$url}}"><li class="active"></li> {{ucwords(Request::segment($i))}}</a></li>
        @endif
      @elseif($i == 1 && $i != count(Request::segments()) )
        <?php $url =$url.'/'.Request::segment($i); ?>
        <li><a href="{{$url}}"><i class="icon-home2 position-left"></i>{{ucwords(Request::segment($i))}}</a></li>
      @else
        @if($i != 0)
          <?php $url =$url.'/'.Request::segment($i); ?>
            <li><a href="{{$url}}">{{Request::segment($i)}}</a></li>
        @endif
      @endif
    @endfor
    </ul>


    <ul class="breadcrumb-elements">
        @yield('right-bradcrump')
    </<ul>
    {{-- <ul class="breadcrumb-elements">
      <li><a href="#"><i class="icon-comment-discussion position-left"></i> Link</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="icon-gear position-left"></i>
          Dropdown
          <span class="caret"></span>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">
          <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
          <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
          <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
          <li class="divider"></li>
          <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
        </ul>
      </li>
    </ul> --}}
  </div>
</div>
