<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/colors.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css">

	@yield('css')
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	{{-- <script type="text/javascript" src="{{asset('js/lib/jquery.min.js')}}"></script> --}}
	{{-- <script type="text/javascript" src="{{asset('js/plugin/core.js')}}"></script> --}}
	{{-- <script type="text/javascript" src="{{asset('js/plugin/jquery.min.js')}}"></script> --}}
	{{-- <script type="text/javascript" src="{{asset('js/plugin/bootstrap.js')}}"></script> --}}
	{{-- <script type="text/javascript" src="{{asset('js/plugin/select2.full.min.js')}}"></script> --}}

	@yield('js_header')

	<!-- /core JS files -->

	<!-- vue JS files -->
	{{-- <script type="text/javascript" src="{{asset('js/app.js')}}"></script> --}}
	<!-- /vue JS files -->

</head>
