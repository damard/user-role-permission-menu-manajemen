<div class="sidebar sidebar-main">
  <div class="sidebar-content">

    <!-- User menu -->
    <div class="sidebar-user">
      <div class="category-content">
        <div class="media">
          <a href="#" class="media-left"><img src="{{Auth::user()->foto ?? 'images/image.png'}}" class="img-circle img-sm" alt=""></a>
          <div class="media-body">
            <span class="media-heading text-semibold">{{Auth::user()->name}}</span>
            <div class="text-size-mini text-muted">
              <i class="icon-pin text-size-small"></i> &nbsp; {{Auth::user()->kecamatan}},
              {{Auth::user()->kabupaten}}, {{Auth::user()->provinsi}}
            </div>
          </div>

          <div class="media-right media-middle">
            <ul class="icons-list">
              <li>
                <a href="#"><i class="icon-cog3"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- /user menu -->


    <!-- Main navigation -->
    <div class="sidebar-category sidebar-category-visible">
      <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

          <!-- Main -->
          @php
            $menu = \App\Models\ViewMenu::distinct()->select('menu_name','link_menu','css_icon','icon_foto','publish')->where('id',Auth::user()->id)->where('publish',true)->get();
          @endphp
          {{-- {{dd($menu)}} --}}
          @foreach ($menu as $key)
            <li><a href="{{$key->link_menu ?? '#'}}">
              @if(isset($key->icon_foto))
                <img class="icon" src="{{ asset($key->icon_foto) }}">
              @elseif(isset($key->css_icon))
                <i class="{{$key->css_icon}}"></i>
              @else
                <i class="icon-home4"></i>
              @endif
              <span>{{$key->menu_name}}</span></a></li>
          @endforeach
        </ul>
      </div>
    </div>
    <!-- /main navigation -->

  </div>
</div>
