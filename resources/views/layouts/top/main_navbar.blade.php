<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html"><img src="/images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
				<li><a class="sidebar-mobile-secondary-toggle"><i class="icon-more"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
				<li><a class="sidebar-control sidebar-secondary-hide hidden-xs"><i class="icon-transmission"></i></a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li>
					<div id="navbar" class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="imgNavSel" src="{{asset('images/flags/'.Session::get('icon'))}}" alt="..." class="img-thumbnail icon-small">  <span id="lanNavSel">Lang</span> <span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu">
											@foreach ($language as $key)
												<li><a id="navIta" href="#" value="$key->id" class="language"> <img id="img{{$key->lang_id}}" src="{{asset('images/flags/'.$key->flag_path)}}" alt="{{$key->lang_name}}" class="img-thumbnail icon-small">  <span id="lang{{$key->lang_id}}">{{$key->lang_name}}</span></a></li>
											@endforeach

        	        </ul>
                </li>
            </ul>
        </div> <!--/.navbar-collapse -->
				</li>

				<li>
					<a href="#">
						<i class="icon-cog3"></i>
						<span class="visible-xs-inline-block position-right">Icon link</span>
					</a>
				</li>

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="/images/image.png" alt="">
						<span>{{\Auth::user()->username??\Auth::user()->name}}</span>
						<i class="caret"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{route('profile.index')}}"><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="#"><i class="icon-coins"></i> My balance</a></li>
						<li><a href="#"><span class="badge badge-warning pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<i class="icon-switch2"></i> Logout </a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
						</form>
							{{-- <a href="#"><i class="icon-switch2"></i> Logout</a></li> --}}
					</ul>
				</li>
			</ul>
		</div>
	</div>
