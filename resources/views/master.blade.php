@include('layouts.header.header')
<body class="sidebar-xs">

	<!-- Main navbar -->
@include('layouts.top.main_navbar')
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
      @include('layouts.navbar.main_sidebar')
			<!-- /main sidebar -->

			<!-- Secondary sidebar -->
      @yield('secondary-navbar')
			<!-- /secondary sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
        @include('layouts.content.main_page')
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Simple panel -->
					<div id="app">
						  @yield('content')
					</div>

					<!-- /simple panel -->


					<!-- Table -->
					<!-- /table -->


					<!-- Grid -->
					<!-- /grid -->




				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<!-- Footer -->
		<div class="navbar navbar-default navbar-fixed-bottom">
			<ul class="nav navbar-nav no-border visible-xs-block">
				<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-up2"></i></a></li>
			</ul>

			<div class="navbar-collapse collapse" id="navbar-second">
				<div class="navbar-text">
					&copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
				</div>

				<div class="navbar-right">
					<ul class="nav navbar-nav">
						<li><a href="#">Help center</a></li>
						<li><a href="#">Policy</a></li>
						<li><a href="#" class="text-semibold">Upgrade your account</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-cog3"></i>
								<span class="visible-xs-inline-block position-right">Settings</span>
								<span class="caret"></span>
							</a>

							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="#"><i class="icon-dribbble3"></i> Dribbble</a></li>
								<li><a href="#"><i class="icon-pinterest2"></i> Pinterest</a></li>
								<li><a href="#"><i class="icon-github"></i> Github</a></li>
								<li><a href="#"><i class="icon-stackoverflow"></i> Stack Overflow</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /footer -->

</body>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
@stack('scripts')
<script type="text/javascript" src="{{asset('js/plugin/them-app.js')}}"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
</html>
